import {Mutation} from '../types';

export default {
  [Mutation.request](state){
    state.status = 'loading'
  },
  [Mutation.success](state, token, user){
    state.status = 'success'
    state.token = token
    state.user = user
  },
  [Mutation.error](state){
    state.status = 'error'
  },
  [Mutation.Logout](state){
    state.status = ''
    state.token = ''
  },

  [Mutation.Islogin](state,islogin){
    state.islogin = islogin
  }


}