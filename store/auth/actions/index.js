import {Action,Mutation} from '../types'
import Axios from 'axios'
import qs from 'qs';
import { thresholdScott } from 'd3';
import { startOfWeek } from 'date-fns';


export default {
     [Action.Login]({commit,state},user){
        return new Promise ((resolve,reject)=>{
            var cred = btoa('my-client'+':'+'my-secret');
            //commit('auth_request')
            Axios.post('http://intranet.lan.go.id:8555/oauth/token',null,
            {
                params:{
                    "grant_type":"password",
                    "username":user.email,
                    "password":user.password
                },
                auth: {
                    username: 'my-client',
                    password: 'my-secret',
                },
                
            }
            )
            .then(res=>{
                const token = res.data.access_token
                const user = res.data.user
                window.$nuxt.$cookies.set('token',token)
                var base64Url = token.split('.')[1]
                var base64 = base64Url.replace(/-/g,'+').replace(/_/g,'/')
                var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c){
                    return '%' + ('00'+ c.charCodeAt(0).toString(16)).slice(-2)
                }).join(''))

                

                console.log(res)
                window.$nuxt.$cookies.set('isijwt',jsonPayload)

                Axios.defaults.headers.common['Authorization']='Basic '+token
                var parsejwt = JSON.parse(jsonPayload)
                console.log(parsejwt.authorities[0])
                if(parsejwt.authorities[0]=="ROLE_LEMDIK"){
                    this.$router.push('/dashboard/diklat')
                }else{
                    this.$router.push('/dashboard/sertifikatDigital')
                }


                
                
                
            })
            .catch(err=> {
                err=> console.warn(err)
                localStorage.removeItem('token')
                reject(err)
            })
        })
    },
    [Action.Login1]({commit},user){
        return new Promise ((resolve,reject)=>{
            Axios('https://intranet.lan.go.id:8444/oauth/token',{
                method:'POST',
                auth:{
                    username: 'cliente',
                    password: 'password'
                },
                data: qs.stringify(
                    {
                        username: user.email,
                        password: user.password,
                        "grant_type": 'password'
                    }
                )
            }).then(res => {
                console.log(res)
                const token = res.data.access_token
                const user = res.data.user
                this.$auth.$storage.setLocalStorage('token',token)
                window.$nuxt.$cookies.set('token',token)
                // localStorage.setItem('token',token)
                Axios.defaults.headers.common['Authorization']='Basic '+token
                this.$router.push('/dashboard')
                // window.$nuxt.$cookies.set('token',token,{})
                resolve(res)
            }).catch(err=>{
                err=> console.warn(err)
                this.$router.push('/login')
                reject(err)
                state.erro
            })
        })
    },

    [Action.parseJWT](token){
        console.log('isi :',token)
        

        //return JSON.parse(jsonPayload)
        return token
    },

    [Action.Logout]({commit,context}){
        window.$nuxt.$cookies.removeAll()
        commit(`${[Mutation.Islogin]}`,false)
        
    }

}