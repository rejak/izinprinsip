import {Action,Mutation} from '../types'
import Axios from 'axios'
import qs from 'qs';
import { thresholdScott } from 'd3';
import { startOfWeek } from 'date-fns';



export default {
     [Action.tambahdiklat]({commit,state},diklat){
        return new Promise ((resolve,reject)=>{

            let form ={
                "instansiPembina" : diklat.instansiPembina,
                "lembagaDiklat" : diklat.lembagaDiklat,
                "namaProgramDiklat" : diklat.namaProgramDiklat,
                "jenisProgramDiklat" : diklat.jenisProgramDiklat,
                "tahun" : diklat.tahun,
                "tempat": diklat.tempat,
                "angkatan" : diklat.angkatan,
                "penyelenggara" : diklat.penyelenggara,
                "tanggalPelaksanaan" : diklat.tanggalPelaksanaan,
                "tanggalSelesai" : diklat.tanggalSelesai,
                "kompetensi" : diklat.kompetensi,
                "persyaratanPeserta" : diklat.persyaratanPeserta,
                "saranaDanPrasarana" : diklat.saranaDanPrasarana,
                "evaluasiPeserta" : diklat.evaluasiPeserta,
                "evaluasiWidyaiswara" : diklat.evaluasiWidyaiswara,
                "evaluasiPenyelenggaraan" : diklat.evaluasiPenyelenggaraan,
                "sertifikasi" : diklat.sertifikasi
            }

            var formdiklat = new FormData()
            formdiklat.append('diklatJson',JSON.stringify(form))
            Axios.post('http://intranet.lan.go.id:8555/diklat/save',formdiklat,
            {
                headers:{
                    "Authorization":'Bearer '+window.$nuxt.$cookies.get('token'),
                    "Content-Type":'application/x-www-form-urlencoded'
                },
            }
            )
            .then(res=>{
                console.log(res)
                this.$router.push('/dashboard/diklat')
            })
            .catch(err=> {
                err=> console.warn(err)
                localStorage.removeItem('token')
                reject(err)
            })
        })
    },

    [Action.daftardiklat]({state,commit},status){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/lemdik',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.semuadiklat]({state,commit},status){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/status/'+status,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.lihatstatusdiklat]({state,commit},status){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/status/'+status,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ubahstatusdiklat]({state,commit},isi){
        var formdiklat = new FormData()

        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/updatestatus/'+isi.diklat+'/'+isi.status,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'),
                     "Content-Type":'application/x-www-form-urlencoded'
                }
            }).then(res=>{
                resolve(res)
                this.$router.push('/dashboard/sertifikatDigital')
            }
            ).catch(err=>{reject(err)
                this.$router.push('/dashboard/sertifikatDigital')
            }
            )
        })
    },

    [Action.uploadpeserta]({state,commit},diklat){
        console.log(window.$nuxt.$cookies.get('token'))
        console.log('isi: '+diklat.diklat)
        var formdiklat = new FormData()
        formdiklat.append('fileSurat',diklat.file)

        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/datapeserta/'+diklat.diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'),
                     "Content-Type":'application/x-www-form-urlencoded'
                },
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.datapeserta]({state,commit},id){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/datapeserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttdlemdik]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatanganlemdik/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttddeputi]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatangandeputi/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttdkepala]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatangankepala/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    
    

}