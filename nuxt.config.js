
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  axios:{
    baseURL:'http://intranet.lan.go.id:8555',
   
    
  },

  proxy:{
    '/oauth/token/' :{
      target: 'http://intranet.lan.go.id:8555'
    }
  },

  auth:{
    strategies:{
      local:{
        endpoints:{
          login:{url:'http://intranet.lan.go.id:8555/oauth/token?grant_type=password&username=user2&password=pass123',method:'post',propertyName:'false',auth:{username: 'my-client',password: 'my-secret'}},
          user:{url:'me',method:'get',propertyName:'data'},
          logout:false
        },
        
      }
    },
    tokenRequired:true,
    tokenType:'Bearer',
    cookie:{
      options:{
        expires:7
      }
    }
  },

  head: {
    title: 'Izin Prinsip',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.6.3/css/all.css' },
      {rel:"stylesheet" ,href:"https://unpkg.com/vue-pay-password/dist/vue-pay-password.css"}
    ]
  },

  router:{
   //middleware : 'auth'
   routes:[
    
   ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/dashboard-plugin.js'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    'vue-warehouse/nuxt',
    'cookie-universal-nuxt',
    '@nuxtjs/proxy',
    'cookie-universal-nuxt',
    'nuxt-vue-multiselect',
    '@nuxtjs/toast'
  ],

  toast: {
    position: 'top-center',
    register: [ // Register custom toasts
      {
        name: 'salah-pass',
        message: 'Email dan Password anda tidak cocok',
        options: {
          type: 'error'
        }
      },
      {
        name: 'login',
        message: 'Login Masuk ...',
        
      },

    ]
},

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
